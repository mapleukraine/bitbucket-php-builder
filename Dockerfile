FROM atlassian/default-image
RUN apt-get update && apt-get install -y php5
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | php -- --quiet \
    && mv composer.phar /usr/local/bin/composer